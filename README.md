# Docker for Symfony4

![Powered by Docker](https://img.shields.io/badge/powered%20by-Docker-blue.svg "Powered by Docker") ![Mit](https://img.shields.io/badge/licence-MIT-green.svg "Mit") ![AutoBuild](https://img.shields.io/badge/build-auto-yellow.svg "AutoBuild")

## [What is Docker?](https://www.docker.com/what-docker)

Docker is the company driving the container movement and the only container platform provider to address every application across the hybrid cloud. Today’s businesses are under pressure to digitally transform but are constrained by existing applications and infrastructure while rationalizing an increasingly diverse portfolio of clouds, datacenters and application architectures. Docker enables true independence between applications and infrastructure and developers and IT ops to unlock their potential and creates a model for better collaboration and innovation.

# Installation

Docker for Symfony4 requires [Docker](https://www.docker.com/) to run.

### Build
Generate your own settings for composer


```sh
$ php BUILD
```

### Run

```sh
$ docker-compose up -d
```

### Create symfony project

```sh
$ docker exec -it freshapps-php-fpm /bin/bash
$ composer create-project symfony/website-skeleton
$ mv my-project/* ./
$ rm -r my-project
```